# USC Task 1 inspection

## Set up 

## Flight time window

### Timeline

#### preflight

#### flight time

1. informed start
   1. FPV had wrong frequnect
   2. fpv battery is dead, fixed fast
   3. didnt focus fpv camera
   4. RFD is not connecting
      1. signal go down to 0 before Condor arrived at the run way
      2. **There was drit biker at the operating area, stopped our timer, so we cant takeoff, and we had time to discuss fixing RFD** 
      3. fixed RFD 900 +
         1. option 1: fly without telemetry
         2. option 2: fix RFD 900
            1. Garikua start fixing 
            2. Reflashed the firmware, with the same configuration and it worked
         3. fixed at 30 minute mark (including pause)
   5. Skeye
      1. Pi cannot connect to pixhawk
      2. pixhawk was not plugged in, powercycled still cannot connect
      3. had to go back to scripting, it worked
      4. lsyncd didn't work
      5. at some point, the connection lost and we cannot trigger the camera
         1. the trigger script stopped
         2. didn't check afterwards
   6. Takeoff at 29 minute after fixing the waypoint speed
   7. mission went well
      1. Auto mode to 100 meter at the start
      2. finish the auto mode when close to the panels
      3. Swtiched to manual mode to do detailed insepections
      4. FPV got cutoff at long range due to direction 
   8. Headset out of battery, and charging after flight

### post flight analysis

1. flight line was well connected after
2. post flight analysis
   1. Map reconstruction and image analysis
   2. completed the analysis
3. found 3 panels, with scale, compass roles, the time window was good post flight

### Communication

it was a good communication link between the ground and air

stop swearing



## Improvement

- be more origanized
- more testing in terms of operations and set ups. 
- operation need  some practice with the order and timing. 
- give people out of flight more info with radio or something.
- get to the flight line eariler
- should have takeoff before 5 minutes
  - package the software better, with internal checks and easier commands
  - 



