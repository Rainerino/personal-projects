# Minute by Minute

## Arrival

![Screenshot from 2019-05-23 16-52-06](/home/yiyi/Pictures/Screenshot from 2019-05-23 16-52-06.png)

> Arrive at 11 am on June 13, 2019, Thursday, Take the loading time of 30 minutes, conservatively leave IAD from subway at 12 am.  

## Subway

From IAD to Branch Avenue Station (12 am to 1:40 pm)

![Screenshot from 2019-05-23 16-56-37](/home/yiyi/Pictures/Screenshot from 2019-05-23 16-56-37.png)

> Metro fares are calculated by how many stops you travel and vary during peak and off-peak hours. During peak hours, most fares range from $2.25 to $6 per trip. During off-peak hours, fares typically range from $1.85 to $3.85. 
>
> Metro riders must pay via [SmarTrip card](https://www.wmata.com/fares/smartrip/). These are plastic, rechargeable fare cards that can be purchased by cash or credit at any Metro station or in advance on [wmata.com](https://www.wmata.com/fares/smartrip/). You can calculate your fare between two stops using Metro’s [Trip Planner](https://www.wmata.com/schedules/trip-planner/).

## Go to Hotel from Branch Avenue Station

> Hotel address: 22211 Three Notch Rd, Lexington Park, MD, 20653 United States of America

### Take Uber ride

- 1:40 pm to 3: 40 pm ![Screenshot from 2019-05-23 16-59-31](/home/yiyi/Pictures/Screenshot from 2019-05-23 16-59-31.png)

- It takes about **1.5hr** to get a appointment, so start booking at the subway.  ![Screenshot from 2019-05-23 17-04-34](/home/yiyi/Pictures/Screenshot from 2019-05-23 17-04-34.png)

### Driving takes about 2 hrs. 
- ![Screenshot from 2019-05-23 17-09-23](/home/yiyi/Pictures/Screenshot from 2019-05-23 17-09-23.png)

### Taxi Service (Back up )
- <https://dcyellowcab.com/>
- <https://www.yelp.ca/search?cflt=taxis&find_loc=Washington%2C+DC>
- ![Screenshot from 2019-05-23 17-11-36](/home/yiyi/Pictures/Screenshot from 2019-05-23 17-11-36.png)

## AVUSI 13rd June Schdule

![Screenshot from 2019-05-23 17-21-05](/home/yiyi/Pictures/Screenshot from 2019-05-23 17-21-05.png)