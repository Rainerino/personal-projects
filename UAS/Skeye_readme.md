# Skeye

----



## Skeye Program Set up

### Overview

This is the module for controlling the camera and transfer images to the ground.

### Set up

> All the below commands starts from the project root directory. 

#### On the Raspberry Pi

General set up

```bash
cd skeye
./tools/setup.sh
```

Set up the python env

```bash
./tools/setup_pip.sh
```

Install Opencv 3 on Pi

> this will take a really long time (30 minutes + on pi 3 B+). Please make sure there is no error

> Dont forget to change the swap size! ELSE IT WON"T COMPILE! [reference](https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/)

```bash
./tools/set_opencv.sh;
./tools/set_opencv_make.sh;
```

> after complete, run this to test installation

```bash
python3 -c "import cv2"
```

Set up ssh between remote and pi

```bash
./tools/setup_ssh.sh
```

Set up Rsync and Lsync(TBA)

### Set up problem

- pipenv fails to install py3exif2:  https://stackoverflow.com/questions/41075975/impossible-to-install-py3exiv2-with-pip

  run:

  ```bash
  sudo apt-get install build-essential
  sudo apt-get install python-all-dev
  sudo apt-get install libexiv2-dev
  sudo apt-get install libboost-python-dev
  ```

- if pipenv run into ```module not callable``` problem:

  ```bash
  pip install pipenv
  pipenv run pip install pip==18.0
  pipenv install
  ```

  

### Running

#### Running with default configuration

> port: 127.0.0.1:14550
>
> baud: 115200
>
> camera: default camera on laptop

This setting is supposed to work with [arudpliot stil simulator](<http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html>). 

```bash
source ./tools/venv/bin/activate
python -m skeye.skeye_main
```

see configuration options at

```bash
python -m skeye.skeye_main -h
```

> Please doublecheck the configuration file!

```bash
code ./rsync_dir/rsysnc.ini
```



| Command                                                      | Feature                   | Note                             |
| ------------------------------------------------------------ | ------------------------- | -------------------------------- |
| ```python -m skeye.skeye_main -p /dev/ttyACM0-b 57200-c sony ``` | Competition configuration | start after vehicle is connected |
|                                                              |                           |                                  |
|                                                              |                           |                                  |

#### Sample running interface



## SkyEye System Set up

---

The configuration used in this examples are: 

> ground IP address: 192.168.1.146
>
> onboard IP address:  192.168.1.103

### SSH Set up

#### Set up the ground computer connection

1. set static ip

   1. Go to network -> ethernet connection -> IPv4 setting
   2. Set the setting to manual and configure it as the follow
   3. Save and connect

   ```bash
   address: 192.168.1.146
   netmask: 255.255.255.0
   gateway: 192.168.0.1
   ```

   

2. ping the pi, you should be getting packets

   ```bash
   ping 192.168.1.103
   ```

#### Set up pi connection

1. Set up the static ip for raspberry pi with this guide:<https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address>

#### Set up SSH

> start this only after a two way connection is set up

1. make sure ssh is enabled

   - UBUNTU 16.04 LTS:

     - ```bash
       sudo apt-get install openssh-server -y
       ```

     - See <https://thishosting.rocks/how-to-enable-ssh-on-ubuntu/>

   - Pi

     - See <https://www.raspberrypi.org/documentation/remote-access/ssh/>

2. Create new key

   - ```bash
     ssh-keygen # if name is id_rsa and id_rsa.pub
     eval `ssh-agent` 
     ssh-add id_rsa
     ```

3. Remove the need for password

   - ```bash
     ssh-keyscan -H ip_name >> .ssh/known_hosts 
     ```

### Lsyncd Set up

See more details at <https://gitlab.com/ubcuas/SkyEye/blob/master/tools/rsync/README.md>

```bash
sudo apt install lsyncd
sudo mkdir /etc/lsyncd
sudo cp lsyncd_pc.conf /etc/lsyncd
sudo mv /etc/lsyncd/lsyncd_pc.conf /etc/lsyncd/lsyncd.conf.lua
sudo mkdir /var/log/lsyncd
cd /var/log/lsyncd
sudo touch {lsyncd.log,lsyncd.stat}

sudo service lsyncd restart
```

##### Sample ground config

```lua
----
-- User configuration file for lsyncd from Pi to computer
--
-- Simple example for default rsync.
--
settings {
    -- we don't need to sync most of the time.
    logfile = "/var/log/lsyncd/lsyncd.log",
    statusFile = "/var/log/lsyncd/lsyncd.stat",
    maxDelays = 1,
    statusInterval = 1,
    nodaemon = false
}

sync {
        default.rsyncssh,
        source="/home/yiyi/Documents/github/sky_eye/rsync_dir",
        targetdir="/home/pi/SkyEye/rsync_dir",
        host="192.168.1.103",
        exclude = {"/processed_image", "/.*"},
        delay = 1,
        rsync={
            rsh = "/usr/bin/ssh -p22 -l pi -i /home/yiyi/.ssh/id_rsa  -o StrictHostKeyChecking=no",
            _extra = {"--delete"},
            archive = true,
            compress = true
        }
}

```



##### Sample onboard config

```lua
----
-- User configuration file for lsyncd from Pi to computer
--
-- Simple example for default rsync.
--
settings {
    -- we don't need to sync most of the time.
    logfile = "/var/log/lsyncd/lsyncd.log",
    statusFile = "/var/log/lsyncd/lsyncd.stat",
    statusInterval = 1,
    nodaemon = false
}

sync {
        default.rsyncssh,
        source="/home/pi/SkyEye/rsync_dir", 
        targetdir="/home/yiyi/Documents/github/sky_eye/rsync_dir",
        host="192.168.1.146",
        exclude = {"/processed_image/.*", "/rsync.ini", "/.*"},
        delay = 1,
        rsync={
            rsh = "/usr/bin/ssh -p22 -l yiyi -i /home/pi/.ssh/id_rsa  -o StrictHostKeyChecking=no",
            _extra = {"--delete"},
            archive = true,
            compress = true
        }
}
```



##### Sample lsyncd.log

###### Success

```tex
Tue Apr 30 14:17:23 2019 Normal: Finished (list): 0
Tue Apr 30 14:17:36 2019 Normal: Rsyncing list
/rsync.ini
Tue Apr 30 14:17:37 2019 Normal: Finished (list): 0
Tue Apr 30 14:41:13 2019 Normal: Rsyncing list
/rsync.ini
Tue Apr 30 14:41:13 2019 Normal: Finished (list): 0
Tue Apr 30 14:41:35 2019 Normal: Rsyncing list
/rsync.ini
Tue Apr 30 14:41:35 2019 Normal: Finished (list): 0
Tue Apr 30 15:20:35 2019 Normal: Rsyncing list
```

###### Fail

```tex
Wed May  1 00:17:00 2019 Normal: --- TERM signal, fading ---
Wed May  1 00:17:38 2019 Normal: recursive startup rsync: /home/yiyi/Documents/SkyEye/rsync_dir/ -> 192.168.1.103:/home/pi/UAS/SkyEye/rsync_dir/ excluding
/.*, /processed_image
ssh: connect to host 192.168.1.103 port 22: Network is unreachable
rsync: connection unexpectedly closed (0 bytes received so far) [sender]
rsync error: unexplained error (code 255) at io.c(226) [sender=3.1.1]
Wed May  1 00:17:38 2019 Error: Temporary or permanent failure on startup of "/home/yiyi/Documents/SkyEye/rsync_dir/". Terminating since "insist" is not set.
```

Sample lsyncd.stat

```tex
Lsyncd status report at Wed May  1 00:17:38 2019

Sync1 source=/home/yiyi/Documents/SkyEye/rsync_dir/
There are 1 delays
active Init 
Excluding:
/.*, /processed_image


Inotify watching 1 directories
  1: /home/yiyi/Documents/SkyEye/rsync_dir/
```



-----



### Problem FAQ

#### TODO

- [ ] Set up PI's auto reconnect script
- [ ] Camera disconneciton check

#### Set up Error

##### SSH fails

```bash
ssh yiyi@192.168.1.146                                                                     ssh: connect to host 192.168.1.146 port 22: Connection refused
```

If u are using Ubuntu, it doesn't come with this!

```bash
sudo apt install openssh-server
```



##### SSH Password on login 

```bash
ssh-copy-id -i .ssh/id_rsa.pub yiyi@192.168.1.146
```



#### Runtime Error

No such file

```bash
Traceback (most recent call last):                                                                                    │
  File "/usr/lib/python3.5/runpy.py", line 193, in _run_module_as_main                                                │
    "__main__", mod_spec)                                                                                             │
  File "/usr/lib/python3.5/runpy.py", line 85, in _run_code                                                           │
    exec(code, run_globals)                                                                                           │
  File "/home/pi/UAS/SkyEye/skeye/skeye_main.py", line 101, in <module>                                               │
    main()                                                                                                            │
  File "/home/pi/UAS/SkyEye/skeye/skeye_main.py", line 57, in main                                                    │
    path_output_dir=IMAGE_DIR, image_count=image_counter)                                                             │
  File "/home/pi/UAS/SkyEye/skeye/camera_core.py", line 142, in take_and_tag_image                                    │
    if not geotag_image(vehicle, path_to_image) and not TEST:                                                         │
  File "/home/pi/UAS/SkyEye/skeye/camera_core.py", line 226, in geotag_image                                          │
    geotag.write_geo_tag(path_to_image, lat, lon, alt, hdg=hdg, roll=roll, pitch=pitch, yaw=yaw)                      │
  File "/home/pi/UAS/SkyEye/GeoTag/geotag.py", line 241, in write_geo_tag                                             │
    img = Image.open(img_path)                                                                                        │
  File "/usr/lib/python3/dist-packages/PIL/Image.py", line 2312, in open                                              │
    fp = builtins.open(filename, "rb")                                                                                │
FileNotFoundError: [Errno 2] No such file or directory: './processed_image/12.JPEG'   
```

##### Connection Lost

- need to power cyce the pi

#### Camera stop taking pictures

```bash
ERROR: Could not capture image.                                                           
ERROR: Could not capture.  
```

Still trying to solve this. It stopped at image 68



