# Platformio Window Setup

## Set up

1. Download vscode
   1. <https://code.visualstudio.com/download>
   2. 
   
2. Download platformio

   1. Method 1: download from vscode:
      1. <https://platformio.org/install/ide?install=vscode>
      2.  <https://platformio.org/platforms/windows_x86>
      3. or use command line:

   ```bash
   pio platform install "windows_x86"
   ```

   2. Method 2: Use python to install (not recommended!)
      1. Check <https://docs.platformio.org/en/latest/installation.html> 
      2. install python 2.7 
      3. Follow the guide

## Run the project

1. create new project and make a LED blink test

![Screenshot from 2019-05-18 20-28-00](/home/yiyi/Pictures/Screenshot from 2019-05-18 20-28-00.png)

2. Copy this code to /src/main.cpp

```c++
/**
 * Blink
 *
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"

#ifndef LED_BUILTIN
#define LED_BUILTIN 13
#endif

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  // turn the LED on (HIGH is the voltage level)
  digitalWrite(LED_BUILTIN, HIGH);

  // wait for a second
  delay(1000);

  // turn the LED off by making the voltage LOW
  digitalWrite(LED_BUILTIN, LOW);

   // wait for a second
  delay(1000);
}
```



2. load the project after downloading the example from <https://github.com/Rainerino/Biyiniao/tree/master/teensy_input>
   1. ![Screenshot from 2019-05-18 20-32-37](/home/yiyi/Pictures/Screenshot from 2019-05-18 20-32-37.png)
   2. Click on the left arrow icon to upload to Teensy. 

## Function Interface

> Code standard: <https://gist.github.com/lefticus/10191322>
>
> See examples at: <https://gitlab.com/ubcuas/airdrop-software>
>
> **NOTE: COPY LIBRARIES FOLDERS TO /lib**

- imu.h

```c++
#ifndef UAS_IMU_H
#define UAS_IMU_H

// include all IMU related libraries here

class UAS_IMU{

    public:
    	// constructors
        UAS_IMU();
        // add some functions, like set up functions, looping functions etc
        double getCurrentHeading();
        
    private:
        // private data that others has no access to
        double current_heading;

};

#endif
```

- imu.cpp

```c++
#include "uas_imu.h"

UAS_IMU::UAS_IMU(){


}
// more function

double UAS_IMU::getCurrentHeading(){

    return -1;
}
```

- main.cpp

```c++
#include <Arduino.h>
#include "imu.h"

UAS_IMU uas_imu;
void setup(){
    uas_imu = UAS_IMU();
    // run set up functions here
}
void loop(){
    double heading = uas_imu.getCurrentHeading();
}
```





