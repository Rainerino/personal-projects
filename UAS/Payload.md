# Payload To Do

---

## Payload 

- Winch design review
- Detail component weights 
  - Plate (1.7kg)
    - Bullet + antenna + Ethernet cable: 180 gram
    - Winch: 
      - Motor: 90 gram
      - Servo:
      - Drum
      - Controller 
  - Rover(0.7kg)
    - waterbottle 
    - frame:
    - Wheel: 

## Payload May 24th 

- clean up winch code to multiple class
- create a speed reader and make some plots on it. Also check of the servo brake the system or not
- create a Winch object class
- add serial communication class
- 

## Payload May 20th 

- [ ] Add window's guide
- [ ] Figure out the serial print bug
- [ ] Complete the controller by reading the speed
- [ ] 

## Payload May 19th Hardware Log

- [ ] Fix Motor Mount
  - [ ] Add a Screw based grid to 
- [x] Fix motor gear
  - [ ] Might just consider a metal one or something
- [ ] Cut the brake, test it out. 
- [ ] Make Rover's Electrical system mounting. 
- [ ] Fix the payload assembly
- [ ] 

## Payload Maty 18th Notes

Rover: GPS problem

TODO: 18th May 2019

- [ ] Measure the weight
- [ ] Finish the controller 
  - [ ] Use RC control to read gps heading
  - [ ] Could be the interference from building
  - [ ] <https://stackoverflow.com/questions/18398998/how-is-heading-calculated-by-gps>
- [ ] Clean up circuit for Winch and Rover
- [ ] Testing!



### Payload May 10th Flight test review

- Summary
  - Test in general is a failure: the airdrop module didn't work as expected, releasing was a pain and the locking mechanism was a very bad idea.
- Mission details: all manual control, wind speed 5 km/h, mostly negligible. 
  - Test 1: Using fake rover, we loitered at 15 meters and trigger the drop. At first rover start to "free fall", against the motor's pulling. at about 5 meters of freefall, manual full brake was engaged and the brake "bite" into the drum due to it's high rotating speed, jammed the drum and stop the drum from any control. We failed to tried to use motor and servo to unjammed the drum, so we landed with payload still 10 meters in the air. (Also, a test 1 only problem: the motor was not secured and fall off the gear train).
  - Test 2: Using fake rover, we loitered at 10 meters and trigger the drop. The brake was responsive enough to be able to bang-bang control it's way down to the ground, then manually release the "rover", and motor was able to winch a loaded rope back up. 
  - Test 3: Using real rover, loiter at 30 meters, the exact same thing from Test 1 happened, same procedure, same result. 
  - Test 4: Using real rover, added extended rope, loiter at 30 meters.  the exact same thing from Test 1 happened. However this time, the extended rope has a weakpoint and the rope snapped at the weakpoint, cause the rover to freefall for 15 meters. 
  - Side test: Rover fall test: With the water bottle, rover weights around 0.7kg. Both of the backwheel (motor wheel) fall off without damage the plastic shaft, the front bearing support broke, let the bearing on the shaft. Corners of the baseplate broke, but the water bottle cage is still intact. 
  - Flight position test: Even though the plate is not balanced, Condor was able to Loiter with stability. Later we introduced oscillation by moving Condor for about 10 meters, and Rover hanging from it with about 5 meters of rope, the oscillation was noticeable and cause instability Condor, but it was adjusted in about 5-10 seconds.  When lowering at various speed and even sudden accelerations, Condor was not effected and there was no oscillation to the drop. 
- Conclusion: 
  - Problems
    - That brake didn't work. 
    - Proof of concept that bang bang control works, but it's very slow. Considering increase the speed cap. 
    - Higher altitude, higher speed tests required before flight test. Failure to complete payload mission is dangerous to Condor. 
    - Improve the gear train, with higher gearing up from Motor to drum to prove more power to slow down the drop. 
    - Manual control is retarded, please get auto mode working. 
  - Proof of concept
    - Drum is big enough to hold 30 meters of ropes
    - Motor and L298N driver is strong enough to get a loaded rope back to the aircraft safely at about 1 m/s speed. 
    - Use Servo brake to stall the payload is not a good idea. 
    - Rover was in a good position to start it's mission when landed, so it's CG was good
- Next To Do
  - Redesign
    - Major redesign to the winch, focusing on the control mechanism (using brake or motor or others)
      - brake: add notch to avoid the jam, but lost the full control of the drop as it currently has
      - motor: main concern is whether the motor is powerful enough to slow it down. 
      - others: consider direct motor to drum control?
  - Re-manufacture
    - reprint rover, multiple of them
    - 
- Next Test
  - Operation flow: 
    - plug in the system
    - Skeye: if there is no connection, skeye will start taking images when camera is plugged in. 
    - winch will once triggered. The Rover will be autonomously released, It will drop 30 meters, wait for 5 seconds, and then retract the rope (assume it's ready)
    - rover will be manually released (on the second stage), as soon as it's release it will try to go to a preset gps location. 
  - Final operation flow. 
    - plug in payload, power winch, Skeye system and plug in the battery for rover. 
    -  winch will be trigger by a transmitter input. 

## Connect Mission Planner to Pi

---

Note that 

### Set up Mission Planner 

- ctrl - f, go to Mavlink
- Set TCP-Host and 57600
- Press start, dont need the write access
- If start doesn't change to stop, close the window and try again.  

### Set up Nginx

- Download nginx on windows <https://www.devdungeon.com/content/nginx-tutorial#install_nginx_on_windows>
- go to nginx/nginx.conf

- ```tex	
  worker_process 1;
  
  events {
  	work_connections 1024;
  }
  stream {
  	server {
  		listen 8071;
  		proxy_pass 127.0.0.1:14550
  	}
  }
  ```

- 



