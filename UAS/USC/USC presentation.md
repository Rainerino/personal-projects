# USC presentation review

---

## General

#### Looking for

- Looking for general form
- System
- Elec
- Mech (spec)
- Software
- approaches
- Team sctructure

#### Take away

- Less text on slides
- Please be short about iterating and focus on the final designs
- Less about ourself(just enough to show them that we are capable)

## ~~UVic~~

## ~~Concordia Uiv~~

## ETS (dronelab)

- ![image](/home/yiyi/Documents/Notes/UAS/USC/IMG_3917.jpg)
- ~ 10 people
- Nutshell
  - Presenting in client based 
  - Considered the budget, provides different soln for different budget
  - platform -> Tasks -> 
- Platform
  - V11 (octocopter) and upgrade
    - 28 kg max weight
    - fully custom
    - more than 100 tests
    - upgraded version: hidden attenuate and more compact. Hide attena inside the landing gear
  - V12
    - most expensive
- Payload
  - 500 MB/s
  - Can use phone to replace the system?
- Tracking
  - 50km range
  - a lot of testing

## Ryerson (RUAV)

- ![mage](/home/yiyi/Documents/Notes/UAS/USC/IMG_3919.jpg)
- 9 people
- image and processing, payload and manufacturing
- Really long personal introduction
- System
  - Triple B
  - SOny $\alpha$5000, 
  - Image processing
    - Themoral
    - image stitched + georeferencing
    - damage anaylsis on stitched images
  - Collect RGM
    - thresh hold
    - bob detection
  - Marker
    - FPV on the marjer
    - Winching device
    - Retrieve with a trap claw?

## U of T Aerial Robot

- ![image](/home/yiyi/Documents/Notes/UAS/USC/IMG_3918.jpg)
- 
- ~ 15 people
- Litearlly introduced everyone
  - do we care that much?
- System
  - BlackHawk
    - Very intersting casing
    - 15 minutes 
    - 10m/s
    - 12.7 km range
    - 6kg, max 10.7 kg
  - Odroid XU4
    - using Ardupliot
    - Mission Planner and Pixhawk
    - 
  - Marker
    - 
  - FPV
    - Runcam
    - Teledyne 
    - BVLOVS
      - Using Lidar for altitude control, and optical flow?!
      - using Antenna mast
      - 
  - Image processing
    - target detection within 30 m
  - Flight path
    - 55 m alt
    - double passing 
    - 5 m width in grid

## Carleton Uiv (Blackbird)

- ~ 10 people
- System
  - Mockingbird
    - Mid ground for UAV
    - As a communication Node
    - Made sure signals are will organized
  - Raven
    - ad
  - Queen
    - Ruler to get close to measure
    - sterei vision
    - using image or video(?) to measure
    - has 3 camera, IR, fpv and gorpo for shared vision
  - FPV
    - with IMU and GPS
    - Gopro, 
  - Compass
    - map the location to the actual map
    - generate Latex report to send
- Detailed operations(no system details)
  - Day 1
    - solar panel damage inspection
    - Survey
  - Day 2
    - inspection

## U of T UTAT (PPT broke)

- mission analysis
- System
  - UTP Airframe
    - rebuild 
    - 4.5 kg payload
  - Payload
    - PX4 flow and Garmin Lidar Lite V3
    - DIGI Ttend 900
    - XIAOMI YI 
    - hacklink fpv
      - 0.5 s latency
    - Ubiquiti Rocket
      - 1km range at least
    - Teledyna Dalsa Nano XL (51 deg FOV)
    - Custom software with MP from last year
    - **Title calibration **
- Tasks analysis
  - 1, 2,3
- Crew demonstration

## ~~Uiv of Manitoba~~

## ~~Ecole Poly~~

