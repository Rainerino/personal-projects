# XBee 

## Set up

### Purchase



### Guide

- Set up guide 1: <https://learn.sparkfun.com/tutorials/exploring-xbees-and-xctu/all>

- Set up guide 2: <https://learn.sparkfun.com/tutorials/xbee-wifi-hookup-guide/all>

- Datasheet: <https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Datasheet.pdf>

- More detailed Specification
  - Series 1: <https://www.sparkfun.com/products/8666>
    - 3.3V @ 50mA
    - 250kbps Max data rate
    - 1mW output (+0dBm)
    - 300ft (100m) range
  - Series 1 Pro: <https://www.sparkfun.com/products/11216>
    - 3.3V @ 215mA
    - 250kbps Max data rate
    - 60mW output (+18dBm)
    - 1 mile (1500m) range
  
- <https://learn.sparkfun.com/tutorials/xbee-shield-hookup-guide/all>

  > A basic guides on setting up with Arduino Shield, and how to test the connection 

- <https://learn.sparkfun.com/tutorials/exploring-xbees-and-xctu/configuring-networks>

  > Configuration Guides on how to set up a Mesh network with XBee.

- <https://www.instructables.com/id/How-to-Use-XBee-Modules-As-Transmitter-Receiver-Ar/>

- <http://www.ardumotive.com/how-to-use-xbee-modules-as-transmitter--receiver-en.html>

  > A detailed guides on how to 

- <https://www.digi.com/blog/digi-xbee-tech-tip-how-to-conduct-an-digi-xbee-range-test/>

  > Guide on How to conduct a range test

- <https://github.com/digidotcom/xbee_ansic_library/blob/master/src/xbee/xbee_io.c>

  > Some libraries
  
- <https://spin.atomicobject.com/2016/07/18/xbee-tutorial/>

  > Really detailed guide

- <http://www.science.smith.edu/dftwiki/index.php/Tutorial:_Arduino_and_XBee_Communication>

  > 

## Software

### Teensy library

> **This library is not completed!**

<https://www.pjrc.com/teensy/td_libs_XBee.html>

### Arduino Set up





