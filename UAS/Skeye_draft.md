# Skeye

### Overview

This is the module for controlling the camera and transfer images to the ground.

### Set up
> All the below commands starts from the project root directory. 

#### On the Raspberry Pi

General set up
```bash
cd skeye
./tools/setup.sh
```

Set up the python env
```bash
./tools/setup_pip.sh
```

Install Opencv 3 on Pi
> this will take a really long time (30 minutes + on pi 3 B+). Please make sure there is no error

> Dont forget to change the swap size! ELSE IT WON"T COMPILE! [reference](https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/)
```bash
./tools/set_opencv.sh;
./tools/set_opencv_make.sh;
```
> after complete, run this to test installation
```bash
python3 -c "import cv2"
```

Set up ssh between remote and pi
```bash
./tools/setup_ssh.sh
```

Set up Rsync and Lsync(TBA)


### Running
> There are two modes: Testing mode and Debugging mode. 
> Testing mode will ignore the vehcile connection and focus on tagging images
> Debug mode will have detailed messages on the system status. Make sure they are turned off when field test

```bash
source ./tools/venv/bin/activate
python -m skeye.skeye_main
```
or 
```bash
./tools/skeye_run.sh
```
see configuration options at
```bash
python -m skeye.skeye_main -h
```

> Please doublecheck the configuration file!
```bash
code ./rsync_dir/rsysnc.ini
```

### PIPENV FAIL
https://stackoverflow.com/questions/41075975/impossible-to-install-py3exiv2-with-pip

### Testing

pipenv problem:
try 
```bash
pip install pipenv
pipenv run pip install pip==18.0
pipenv install
```
if pipenv run into ```module not callable``` problem